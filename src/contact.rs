use serde_derive::{Serialize, Deserialize};
use sodiumoxide::crypto::kx::PublicKey;

#[derive(Serialize, Deserialize, Debug)]
pub struct Contact {
    pub registration: String,
    pub public_key: PublicKey,
}

impl Contact {
    pub fn new(registration: &str, public_key: PublicKey) -> Contact {
        return Contact {
            registration: String::from(registration),
            public_key: public_key,
        };
    }
}