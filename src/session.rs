use serde_derive::{Serialize, Deserialize};

use sodiumoxide::crypto::kx;
use sodiumoxide::crypto::kx::{SecretKey, PublicKey, SessionKey};

use crate::identity::Identity;
use crate::prekey::PreKeyBundle;


#[derive(Serialize, Deserialize, Debug)]
pub struct Session {
    identity: Identity,
    prekey_bundle: PreKeyBundle,
    ephemeral_public_key: PublicKey,
    ephemeral_secret_key: SecretKey,
}

impl Session {
    pub fn new(identity: Identity, prekey_bundle: PreKeyBundle) -> Session {
        let (ephemeral_pk, ephemeral_sk) = kx::gen_keypair();

        return Session {
            identity: identity,
            prekey_bundle: prekey_bundle,
            ephemeral_public_key: ephemeral_pk,
            ephemeral_secret_key: ephemeral_sk,
        };
    }

    // DH1 = DH(IKA, SPKB)
    // DH2 = DH(EKA, IKB)
    // DH3 = DH(EKA, SPKB)
    // DH4 = DH(EKA, OPKB)
    // SK = KDF(DH1 || DH2 || DH3 || DH4)
    pub fn get_secret(&self) {
        let spkb = &self.prekey_bundle.signed_prekey;
        let ikb = &self.prekey_bundle.identity_key;
        let opkb = &self.prekey_bundle.one_time_prekey;

        let ika_pk = &self.identity.public_key;
        let ika_sk = &self.identity.secret_key;
        let eka_pk = &self.ephemeral_public_key;
        let eka_sk = &self.ephemeral_secret_key;

        let dh1 = kx::client_session_keys(ika_pk, ika_sk, spkb).unwrap();
        let dh2 = kx::client_session_keys(eka_pk, eka_sk, ikb).unwrap();
        let dh3 = kx::client_session_keys(eka_pk, eka_sk, spkb).unwrap();
        let dh4 = kx::client_session_keys(eka_pk, eka_sk, opkb).unwrap();

        println!("{:?}", dh1);
        println!("{:?}", dh2);
        println!("{:?}", dh3);
        println!("{:?}", dh4);
    }
}