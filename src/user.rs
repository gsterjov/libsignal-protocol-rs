use crate::identity::Identity;
use crate::prekey::OneTimePreKeys;
use crate::key_store::KeyStore;
use crate::client::Client;
use crate::message::Message;
use crate::session::Session;


pub struct User<T, K> {
    registration: String,
    key_store: T,
    client: K,
}

impl<T: KeyStore, K: Client> User<T, K> {
    pub fn new(registration: &str, store: T, client: K) -> User<T, K> {
        return User {
            registration: String::from(registration),
            key_store: store,
            client: client,
        };
    }

    pub fn register(&self) {
        self.create_identity();
        self.create_prekeys();
    }

    pub fn create_identity(&self) {
        let identity = Identity::new(&self.registration);
        identity.save(&self.key_store);
        self.client.register(&identity);
    }

    pub fn create_prekeys(&self) {
        let prekeys = OneTimePreKeys::new(&self.registration);
        prekeys.save(&self.key_store);
        self.client.add_prekeys(&prekeys);
    }

    pub fn send_message(&self, recipient: &str, message: &str) {
        let identity = Identity::load(&self.registration, &self.key_store);
        let prekey_bundle = self.client.get_prekey_bundle(recipient);
        let session = Session::new(identity, prekey_bundle);
        println!("{:?}", session);
        session.get_secret();
//        let contact = self.client.get_contact(&recipient);
//        let message = Message::new(&identity, &contact, &message);
//        self.client.send_message(&message);
    }

    pub fn get_message(&self) -> String {
        let identity = Identity::load(&self.registration, &self.key_store);
        let message = self.client.get_message(&self.registration);
        let contact = self.client.get_contact(&message.sender);

        return message.decrypt(&identity, &contact);
    }
}