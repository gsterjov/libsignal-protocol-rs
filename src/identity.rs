use serde_derive::{Serialize, Deserialize};

use sodiumoxide::crypto::kx;
use sodiumoxide::crypto::kx::{SecretKey, PublicKey, SessionKey};

use crate::key_store::KeyStore;
use crate::contact::Contact;


#[derive(Serialize, Deserialize, Debug)]
pub struct Identity {
    pub registration: String,
    pub public_key: PublicKey,
    pub secret_key: SecretKey,
}

impl Identity {
    pub fn new(registration: &str) -> Identity {
        let (ident_pk, ident_sk) = kx::gen_keypair();

        let identity = Identity {
            registration: String::from(registration),
            public_key: ident_pk,
            secret_key: ident_sk,
        };

        return identity;
    }

    pub fn load<T: KeyStore>(registration: &str, store: &T) -> Identity {
        return store.get(&format!("{}_identity", registration));
    }

    pub fn save<T: KeyStore>(&self, store: &T) {
        store.set(&format!("{}_identity", self.registration), &self);
    }
    
    pub fn new_session(&self, recipient: &Contact) -> (SessionKey, SessionKey) {
        return kx::client_session_keys(&self.public_key, &self.secret_key, &recipient.public_key).unwrap();
    }

    pub fn new_server_session(&self, sender: &Contact) -> (SessionKey, SessionKey) {
        return kx::server_session_keys(&self.public_key, &self.secret_key, &sender.public_key).unwrap();
    }
}