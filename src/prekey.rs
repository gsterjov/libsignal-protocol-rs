use std::collections::HashMap;

use serde_derive::{Serialize, Deserialize};

use sodiumoxide::crypto::kx;
use sodiumoxide::crypto::kx::{SecretKey, PublicKey};

use crate::key_store::KeyStore;


#[derive(Serialize, Deserialize, Debug)]
pub struct OneTimePreKeys {
    pub registration: String,
    pub keys: HashMap<PublicKey, SecretKey>,
}

impl OneTimePreKeys {
    pub fn new(registration: &str) -> OneTimePreKeys {
        let mut keys = HashMap::with_capacity(100);

        for _ in 0..100 {
            let (public_key, secret_key) = kx::gen_keypair();
            keys.insert(public_key, secret_key);
        }

        return OneTimePreKeys {
            registration: String::from(registration),
            keys: keys,
        };
    }

    pub fn load<T: KeyStore>(registration: &str, store: &T) -> OneTimePreKeys {
        return store.get(&format!("{}_prekeys", registration));
    }

    pub fn save<T: KeyStore>(&self, store: &T) {
        store.set(&format!("{}_prekeys", self.registration), &self);
    }
}


#[derive(Serialize, Deserialize, Debug)]
pub struct PreKeyBundle {
    pub identity_key: PublicKey,
    pub signed_prekey: PublicKey,
    pub one_time_prekey: PublicKey,
}
