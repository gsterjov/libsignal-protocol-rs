use serde_derive::{Serialize, Deserialize};

use sodiumoxide::crypto::secretbox;
use sodiumoxide::crypto::secretbox::{Key, Nonce};

use crate::identity::Identity;
use crate::contact::Contact;


#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    pub sender: String,
    pub recipient: String,
    pub nonce: Vec<u8>,
    pub payload: Vec<u8>,
}

impl Message {
    pub fn new(sender: &Identity, recipient: &Contact, message: &str) -> Message {
        let (_rx, tx) = sender.new_session(&recipient);

        let nonce = secretbox::gen_nonce();
        let key = Key::from_slice(&tx[..]).unwrap();
        let payload = secretbox::seal(message.as_bytes(), &nonce, &key);

        let message = Message {
            sender: sender.registration.clone(),
            recipient: recipient.registration.clone(),
            nonce: nonce.as_ref().to_vec(),
            payload: payload,
        };

        return message;
    }

    pub fn decrypt(&self, identity: &Identity, sender: &Contact) -> String {
        let nonce = Nonce::from_slice(&self.nonce).unwrap();

        let (rx, _tx) = identity.new_server_session(&sender);
        let key = Key::from_slice(&rx[..]).unwrap();
        let decrypted = secretbox::open(&self.payload, &nonce, &key).unwrap();

        return String::from_utf8(decrypted).unwrap();
    }
}