use serde::Serialize;
use serde::de::DeserializeOwned;

pub trait KeyStore {
    fn set<T: Serialize>(&self, name: &str, key: &T);
    fn get<T: DeserializeOwned>(&self, name: &str) -> T;
}