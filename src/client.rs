use crate::identity::Identity;
use crate::prekey::{OneTimePreKeys, PreKeyBundle};
use crate::contact::Contact;
use crate::message::Message;

pub trait Client {
    fn register(&self, identity: &Identity);
    fn add_prekeys(&self, prekeys: &OneTimePreKeys);
    fn get_prekey_bundle(&self, registration: &str) -> PreKeyBundle;
    fn get_contact(&self, registration: &str) -> Contact;
    fn send_message(&self, message: &Message);
    fn get_message(&self, registration: &str) -> Message;
}